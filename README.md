# Telegram-Turkiye
Telegram Türkçe Grup,Kanal Listesi
###### Gruplara katılarak grupların kurallarını kabut etmiş sayılırsınız!

## Gruplar:

* AnimeTR: https://t.me/anime_turkiye
* TPR: https://t.me/joinchat/IL9JgUbSDYzD61v0GBUiAg
* DroidTR: https://t.me/droidtr
* Teknolojiden Anlayanlar: https://t.me/joinchat/I8iC1Et8KByUJQTPWbmwoA
* Huawei Türkiye: https://t.me/joinchat/Ik6ZPkjZCvaf7CpCi134sw
* Türkiye Bilişimi Kalkındırma Topluluğu: https://t.me/tbktresmi
* ElektronikTR: https://t.me/elektroniktr
* General Mobile 4G/5 ve Wileyfow Swift: https://t.me/cracklinturkiye
* Ses - Müzik TR: https://t.me/SESTR_TR
* j2me jar Oyun Program - J2ME Java Micro Edition: https://t.me/j2megrup
* PythonTR: https://t.me/Python_TR
* C#/Unity grubu: https://t.me/Csharp_TR
* Java_TR: https://t.me/Java_TR
* Linux_TR: https://t.me/Linux_TR
* p_sohbet: https://t.me/p_sohbet

## Kanallar:
* Anime & Fan-art: https://t.me/fan_artanime
* Fun4Telegram: https://t.me/mim4turkey
* Old Game Archive: https://t.me/joinchat/AAAAAEqxcuvF-cccHD3RfA
* PlayBoy Store: https://t.me/PlayBoyStore
* Cats: https://t.me/catdamnit
* Software Notes (Yazılım notları): https://t.me/YazilimNotlari
* 64Bit Teknoloji(TR):https://t.me/altmisdortbit
* HI-FI Music: https://t.me/dooplaylist
* Telegram Türkçe: https://t.me/trtelegram
* Crackling Downloads: https://t.me/crackling_downloads
* Project Crackling Channel: https://t.me/projectcrackling
* Samsung Galaxy Gio Depolama bölümü: https://t.me/joinchat/AAAAAE9IMeAu1x-_geHTJQ
* Java Apps & Games: https://t.me/javaapps
* ReactXP: https://t.me/ReactXP
* Biomedikal/Sağlık Döküman: https://t.me/medicaldocuments
* Android x86: https://t.me/aandroidx86
* Ses - Müzik Genel: https://t.me/SESTR_TR
* Kudurun™: https://telegram.me/Kudurun
